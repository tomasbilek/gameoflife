import java.util.Scanner;

public class GameOfLife {
    private static final int NONE = 0;
    private static final int ALIVE = 1;
    private static final int DEAD = 2;

    private static int generateRandomState() {
        return (int) (Math.random() * (2));
    }

    private static int[][] generateBoard(int n, int m) {
        int[][] board = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                board[i][j] = generateRandomState();
            }
        }
        return board;
    }

    private static void printBoard(int[][] board) {
        for (int[] boardRow : board) {
            for (int boardColumnElement : boardRow) {
                switch (boardColumnElement) {
                    case NONE:
                        System.out.print(".");
                        break;
                    case ALIVE:
                        System.out.print("*");
                        break;
                    default:
                        System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static int whatIsAround(int[][] board, int x, int y, int lookFor) {
        int lower_x = 0;
        int upper_x = board.length;

        int lower_y = 0;
        int upper_y = board[0].length;

        // We need to look at neighbour;
        if (x > 0) {
            lower_x = x - 1;
        }
        if (x < upper_x - 1) {
            upper_x = x + 1;
        }
        if (y > 0) {
            lower_y = y - 1;
        }
        if (y < upper_y - 1) {
            upper_y = y + 1;
        }

        int counter = 0;
        for (int i = lower_x; i < upper_x; i++) {
            for (int j = lower_y; j < upper_y; j++) {
                if (i == x || j == y) {
                    continue;
                }
                if (board[i][j] == lookFor) {
                    counter++;
                }
            }
        }
//        System.out.println("Counter pro souradnici: {" + x + ", " + y + "} => " + counter + " (" + lookFor + ")");
        return counter;
    }

    private static int[][] simulationStep(int[][] board) {
        int[][] newBoard = new int[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                final int howManyOfAlive = whatIsAround(board, i, j, ALIVE);
                final int howManyOfDead = whatIsAround(board, i, j, DEAD);
                final int howManyOfNothing = whatIsAround(board, i, j, NONE);

                // Generate new values based on rules
                if (howManyOfAlive == 4 && howManyOfDead > 3) {
                    board[i][j] = ALIVE;
                } else if (howManyOfAlive < 3 && howManyOfDead == 0) {
                    board[i][j] = DEAD;
                }
            }
        }
        return newBoard;
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // Get board dimensions
        System.out.println("Zadejte velikost desky N (výška):");
        final int n = in.nextInt();

        System.out.println("Zadejte velikost desky M (šířka):");
        final int m = in.nextInt();

        // Get number of simulation steps
        System.out.println("Zadejte počet kroků:");
        int numberOfSteps = in.nextInt();

        // Generate initial board
        int[][] board = generateBoard(n, m);

        // Print first step
        System.out.println("Vychozi stav");
        printBoard(board);

        for (int i = 0; i < numberOfSteps; i++) {
            System.out.println("\tKrok: " + (i + 1));

            // Do life simulation step
            board = simulationStep(board);

            // Print board and iterate again
            printBoard(board);
            System.out.println();
        }

        System.out.println("\nKonec");
    }


}
